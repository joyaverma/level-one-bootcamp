#include <stdio.h>
int input()
{
    int num;
    printf("Enter the number of elements to be added\t");
    scanf("%d",&num);
    return num;
}
void input_array(int n,int a[n])
{
    printf("Enter the numbers to be added:\n");
    for(int i=0;i<n;i++)
    scanf("%d",&a[i]);
}
int compute(int n,int a[n])
{
    int sum=0;
    for(int i=0;i<n;i++)
    sum+=a[i];

    return sum;
}
int output(int sol)
{
    int i;
    printf("The sum of n numbers is %d \n",sol);
    return sol;
}
int main()
{
    int n,sol;
    n=input();
    int a[n];
    input_array(n,a);
    sol=compute(n,a);
    output(sol);
}