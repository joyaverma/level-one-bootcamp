#include<stdio.h>
#include<math.h>
float X(float x1,float x2)
{
float a;
a=(x2-x1)*(x2-x1);
return a;
}
float Y(float y1,float y2)
{
float b;
b=(y2-y1)*(y2-y1);
return b;
}
float dist(int x1,int y1,int x2,int y2)
{
float d;
d=sqrt(X(x1,x2)+Y(y1,y2));
return d;
}
int main()
{
float x1,x2,y1,y2,res;
printf("Enter the coordinates of X axis(x1,y1):");
scanf("%f%f",&x1,&y1);
printf("Enter the coordinates of Y axis(x2,y2):");
scanf("%f%f",&x2,&y2);
res=dist(x1,y1,x2,y2);
printf("distance between two points is %.2f",res);
return 0;
}