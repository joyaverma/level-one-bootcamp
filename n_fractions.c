#include<stdio.h>
typedef struct fraction
{
    int num;
    int deno;
}Fraction;
int input_n()
{
    int n;
    printf("Enter the number of fractions to be added:");
    scanf("%d",&n);
    return n;
}
Fraction input_fraction(int n,Fraction a[n])
{
	for(int i=0;i<n;++i)
	{
	printf("Enter the fraction %d (numerator and denominator):",i+1);
	scanf("%d%d",&a[i].num,&a[i].deno);
	}
}
Fraction sum(int n,Fraction a[n])
{
    Fraction r;
    Fraction temp;
    int i;
	int numerator=0,denominator=1;
	for(i=0;i<n;i++)
	{
	denominator*=a[i].deno;
	}
	for(i=0;i<n;i++)
	{
	numerator+=(a[i].num)*(denominator/a[i].deno);
	}
	temp.num=numerator;
	temp.deno=denominator;
	int gcd;
	for(i=1;i<=temp.num && i<=temp.deno;i++)
	{
	    if(temp.num%i==0 && temp.deno%i==0)
	        {
	            gcd=i;
	        }
	}
	r.num=temp.num/gcd;
	r.deno=temp.deno/gcd;
	return r;
}
void result(int n,Fraction a[n],Fraction r)
{
	int i;
	printf("The sum of ");
	for(i=0;i<n-1;++i){
	    printf("%d/%d and ",a[i].num,a[i].deno);
	}
	printf("%d/%d is %d/%d",a[i].num,a[i].deno,r.num,r.deno);

}
int main()
{
	int n;
	Fraction fraction_result;
	Fraction a[n];
	n=input_n();
	input_fraction(n,a);
	fraction_result=sum(n,a);
	result(n,a,fraction_result);
	return 0;

}
//WAP to find the sum of n fractions.