#include<stdio.h>
#include<math.h>
struct coordinate
{
int X,Y;
};
float getdist(struct coordinate x1,struct coordinate y1,struct coordinate x2,struct coordinate y2)
{
float d;
d=sqrt(((x2.Y-x1.X)*(x2.Y-x1.X))+((y2.Y-y1.X)*(y2.Y-y1.X)));
return d;
}
int main()
{
struct coordinate x1,y1,x2,y2;
printf("Enter the coordinates of X:");
scanf("%d%d",&x1.X,&y1.X);
printf("Enter the coordinates of Y:");
scanf("%d%d",&x2.Y,&y2.Y);
printf("The distance between X and Y is %.2f",getdist(x1,y1,x2,y2));
return 0;
}